import { Telegraf } from 'telegraf'
import { ExtraReplyMessage } from 'telegraf/typings/telegram-types'

export type TelegramBotSendMessageOptionsType = {
    telegramApiToken: string
    chatId: string | number
    message: string
    extraOptions?: ExtraReplyMessage
}

export class TelegramBot {
    private readonly bot: Telegraf

    constructor(apiToken: string) {
        this.bot = new Telegraf(apiToken)
    }

    public init() {
        this.bot.start((ctx) => this.onStart(ctx))

        this.bot.launch()
    }

    private onStart(ctx: any) {
        console.log(ctx)
    }

    public static async sendMessage(options: TelegramBotSendMessageOptionsType) {
        const bot = new Telegraf(options.telegramApiToken)

        return bot.telegram.sendMessage(options.chatId, options.message, options.extraOptions)
    }
}
