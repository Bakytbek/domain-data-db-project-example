type ConfigType = {
    mainMongoConnectionUrl: string
    redisUrl: string
    telegramApiToken: string
}

export const Config: ConfigType = {
    mainMongoConnectionUrl: 'MAIN-MONGO-CONNECTION-URL',
    redisUrl: 'REDIS-URL',
    telegramApiToken: 'TELEGRAM-API-TOKEN',
}
