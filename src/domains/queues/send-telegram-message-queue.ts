import Queue from 'bull'
import { Config } from '../config'
import { ExtraReplyMessage } from 'telegraf/typings/telegram-types'

type JobType = {
    chatId: string
    message: string
    extraOptions?: ExtraReplyMessage
}

export const sendTelegramMessageQueue = new Queue<JobType>('send-telegram-message-queue', Config.redisUrl)
