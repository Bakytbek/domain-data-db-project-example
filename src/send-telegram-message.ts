import { initDatabase } from './db/init-database'
import { Config } from './domains/config'
import { sendTelegramMessageQueue } from './domains/queues/send-telegram-message-queue'
import { TelegramBot } from './domains/telegram-bot'

const start = (): void => {
    const numberOfWorkers = 1
    sendTelegramMessageQueue.process(numberOfWorkers, async (job, done) => {
        try {
            const { chatId, message, extraOptions } = job.data

            const response = await TelegramBot.sendMessage({
                telegramApiToken: Config.telegramApiToken,
                chatId,
                extraOptions,
                message,
            })

            console.log(response)

            done(null)
        } catch (e: any) {
            console.log(e)
            done(new Error(e))
        }
    })
}

initDatabase({ main: true }).then(start)
