import { initDatabase } from './db/init-database'
import { Config } from './domains/config'
import { TelegramBot } from './domains/telegram-bot'

const start = async (): Promise<void> => {
    const bot = new TelegramBot(Config.telegramApiToken)

    bot.init()
}

initDatabase({ main: true }).then(start)
